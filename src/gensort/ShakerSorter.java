package gensort;

public class ShakerSorter extends abstractSort {

    @Override
    <T extends Comparable<T>> T[] sort(T[] pole) {
        for (int i = 0; i < pole.length / 2; i++) {
            boolean prohozeno = false;
            for (int j = i; j < pole.length - i - 1; j++) {
                if (pole[j].compareTo(pole[j + 1]) > 0) {
                    T tmp = pole[j];
                    pole[j] = pole[j + 1];
                    pole[j + 1] = tmp;
                    prohozeno = true;
                }
            }
            for (int j = pole.length - 2 - i; j > i; j--) {
                if (pole[j].compareTo(pole[j - 1]) < 0) {
                    T tmp = pole[j];
                    pole[j] = pole[j - 1];
                    pole[j - 1] = tmp;
                    prohozeno = true;
                }
            }
            if (!prohozeno) {
                break;
            }
        }
        return pole;
    }

}
