package gensort;

public class SelectionSorter extends abstractSort {

    @Override
    <T extends Comparable<T>> T[] sort(T[] pole) {
        for (int i = 0; i < pole.length - 1; i++) {
            int nejvetsiPozice = i;
            for (int j = i + 1; j < pole.length; j++) {
                if (pole[j].compareTo(pole[nejvetsiPozice]) < 0) {
                    nejvetsiPozice = j;
                }
            }
            T tmp = pole[i];
            pole[i] = pole[nejvetsiPozice];
            pole[nejvetsiPozice] = tmp;
        }
        return pole;
    }

}
