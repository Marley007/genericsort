package gensort;

public class BubbleSorter extends abstractSort {

    @Override
    public <T extends Comparable<T>> T[] sort(T[] pole) {
        for (int i = 0; i < pole.length - 1; i++) {
            for (int j = 0; j < pole.length - i - 1; j++) {
                if (pole[j].compareTo(pole[j + 1]) > 0) {
                    T tmp = pole[j];
                    pole[j] = pole[j + 1];
                    pole[j + 1] = tmp;
                }
            }
        }
        return pole;
    }

}
