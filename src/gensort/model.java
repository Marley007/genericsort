package gensort;

import java.util.Observable;

public class model extends Observable {

    private String vraceni;

    public model() {
        this.vraceni = "";
    }

    public <T> void SelectionSort(String cisla) {
        String pole[] = cisla.split(",");
        this.createResult(new SelectionSorter().sort(pole));
    }

    public <T> void InsertionSort(String cisla) {
        String pole[] = cisla.split(",");
        this.createResult(new InsertionSorter().sort(pole));
    }

    public <T> void BubbleSort(String cisla) {
        String pole[] = cisla.split(",");
        this.createResult(new BubbleSorter().sort(pole));
    }
    
    public <T> void ShakerSort(String cisla) {
        String pole[] = cisla.split(",");
        this.createResult(new ShakerSorter().sort(pole));
    }

    public <T> void createResult(T pole[]) {
        int delkaPole = pole.length;
        for (T value : pole) {
            this.vraceni += String.valueOf(value);
            if (--delkaPole != 0) {
                this.vraceni += ", ";
            }
        }
        setChanged();
        notifyObservers();
    }

    public String getVraceni() {
        return vraceni;
    }

}
