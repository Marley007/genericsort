package gensort;

public abstract class abstractSort {

    abstract <T extends Comparable<T>> T[] sort(T[] pole);
}
